<HTML><HEAD>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-2">
<META content="MW" name=GENERATOR></HEAD>
<body>
<font face="Times New Roman">
<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#E9E3C1">
<tr><td width="120"> <img src="graph/11.jpg" width="120" height="120"></td>
<td width="100%" background="graph/21.jpg"><br></td>
<td width="120"> <img src="graph/31.jpg" width="120" height="120"></td></tr>
<td width="120" background="graph/12.jpg"><br></td>
<td width="100%" background="graph/22.jpg"><BR>
<CENTER><FONT size=6>
Joanna Chmielewska&nbsp; &nbsp;
<A href="tyt.htm"><IMG height=32 src="graph/strz_gor.gif" width=32 border=0></A>&nbsp; &nbsp;
Harpie
<P><IMG height=16 src="graph/line16.gif" width=700 border=0></P>
<A href="037.htm"><IMG height=32 src="graph/strz_lew.gif" width=32 border=0></A>
<IMG height=32 src="graph/bnc_anm.gif" width=32 border=0>
&nbsp; &nbsp; &nbsp; 038 &nbsp; &nbsp; &nbsp;
<IMG height=32 src="graph/bnc_anm.gif" width=32 border=0>
<A href="039.htm"><IMG height=32 src="graph/strz_prw.gif" width=32 border=0></A>
<P><IMG height=16 src="graph/line16.gif" width=700 border=0>
</CENTER><small>
<P align=justify>&nbsp; &nbsp; &nbsp; <BIG><BIG>I</BIG></BIG>nformacji na sekretarce Bie�an wys�ucha� dopiero po powrocie do domu oko�o wp� do drugiej w nocy. Spodoba�a mu si�. Z wysi�kiem powstrzyma� si� od natychmiastowego rozpocz�cia dzia�a�.
<P align=justify>&nbsp; &nbsp; &nbsp; Dwa kolorowe zdj�cia Wandzi z Andrzejem polecia�y do Stan�w Zjednoczonych nazajutrz rano. Zwa�ywszy, i� o tej porze Wojciechowski z kolei spa� b�ogim snem, odpowiedzi mo�na by�o oczekiwa� dopiero w godzinach popo�udniowych. Obaj z Robertem zacz�li zatem dzie� pracy od konfrontacji osi�gni��.
<P align=justify>&nbsp; &nbsp; &nbsp; - Dr��kiewicz przysi�g�, �e o pani Parker dowiedzia� si� w pi�tek, znaczy w dniu zab�jstwa - relacjonowa� Robert. - Melania si� ni� nie chwali�a, ani jemu, ani nikomu. Nie musia�a, oni i tak spotykaj� si� na mie�cie, albo u niego, wi�c nawet stado przyjezdnych krewnych nic by im nie zaszkodzi�o. On za� tak znowu g�by nie rozpuszcza, owszem, przemy�liwa, �eby si� z ni� o�eni�, teraz nawet bardziej ni� przedtem, ale ona nie chce. A nie rozpuszcza, bo m�odszy od niej i wiadomo, jak na to ludzie reaguj�. Facetka jest atrakcyjna, zrobi�a si� bogata, namawia�by j�, tylko jest jeden k�opot, mianowicie chcia�by mie� dzieci. Ona mu nie urodzi, to wyra�nie powiedzia�a. Przy kielichu siedzieli�my, roz�o�y� si� troch� i przyzna�, �e najbardziej by chcia�, �eby jaka� dziewczyna mu co� urodzi�a, jego w�asne, o�eni� si� z Melani� i niechby to co� �y�o w dobrobycie. W�tpi, czy Melania si� zgodzi. Wi�c jej ca�y maj�tek ni go zi�bi, ni grzeje, chocia� owszem, przyznaje, �e za jej fors� du�o drobiazg�w leci. Samoch�d chocia�by.
<P align=justify>&nbsp; &nbsp; &nbsp; - Teraz b�dzie mia�a wi�cej - przypomnia� Bie�an.
<P align=justify>&nbsp; &nbsp; &nbsp; - On te� to rozumie. Mo�e gdzie� pojad�, na jaki urlop albo co. Na reporta�e. Gdyby si� z ni� o�eni�, rezygnuj�c z dzieci, ona starsza, po �onie by dziedziczy�, ale te� w�tpi, bo nie wiadomo komu z brzega. Rozmawia�, nie rozmawia� na ten temat z nikim absolutnie, g�upio by mu by�o. Rozwa�a spraw� w aspektach osobistych sam ze sob� i na tym koniec. Do ostatecznych wniosk�w jeszcze nie doszed�, a ja si� mam nie dziwi�, �e od owego pi�tku Melania wyda�a mu si� m�odsza i pi�kniejsza.
<P align=justify>&nbsp; &nbsp; &nbsp; - I mam nadziej�, �e mu si� nie dziwisz - podsumowa� Bie�an. - Tak podejrzewa�em, �e ona babci nie reklamowa�a, wi�c po�ytku z niego wielkiego nie mamy. Przyzna� si�, co robili na g�rze?
<P align=justify>&nbsp; &nbsp; &nbsp; - Naprawd� wybrali zdj�cie do publikacji, a potem troch� tego... no, pogzili si� przez chwil�. Dlatego on zszed� na d�, a ona zosta�a poprawi� twarz i uczesanie, bo siostry ma spostrzegawcze. Jego zdaniem, wnioskuj�c z efekt�w, na zabijanie kogokolwiek zabrak�oby jej czasu, ale to i tak, zdaje si�, mamy om�wione. Podejrzanych d�wi�k�w nie s�ysza�. Na ten temat w og�le ulewa�o si� z niego ca�kiem swobodnie, stopowa�o go troch� przy gadaniu o pracy, o kim, jak, pod czyim wp�ywem, pisa�, nie pisa� i tak dalej. To nas nie dotyczy, gnojowisko og�lne.
<P align=justify>&nbsp; &nbsp; &nbsp; Bie�an kiwn�� g�ow� i wyst�pi� ze swoj� relacj�.
<P align=justify>&nbsp; &nbsp; &nbsp; - Notariusz by� lepszy. Te� nie ukrywam, �e siedzieli�my przy koniaczku. Sekretarki ma sprawdzone, tylko jedna m�oda, ale za to c�rka przyjaci�. Nigdy dotychczas z jego kancelarii ani jedno s�owo nie wysz�o, ale za klient�w gwarantowa� nie mo�e. Taki ma g�upi uk�ad przestrzenny, �e w r�nych miejscach r�ne rzeczy s�ycha�. Je�li kto� m�wi cicho, wszystko jest w porz�dku, ale pani Parker nie kr�powa�a si�, a g�os mia�a przenikliwy. Siedzia�o mu wtedy w biurze troje klient�w, dwie sztuki w poczekalni, jedna w sekretariacie, gdzie urz�duje tak�e jego zast�pca, mogli s�ysze� ka�de s�owo. Zaraz... no, za p� godziny... powinien zadzwoni� i poda� mi ich nazwiska, to na wszelki wypadek. Pomijaj�c jego samego i jego personel, s� to jedyne osoby, kt�re z detalami mog�y pozna� ostatni� wol� denatki.
<P align=justify>&nbsp; &nbsp; &nbsp; Robert z pow�tpiewaniem patrzy� na zwierzchnika.
<P align=justify>&nbsp; &nbsp; &nbsp; - Je�eli �adna z tych os�b nie by�a blisk� rodzin� spadkobiercy...
<P align=justify>&nbsp; &nbsp; &nbsp; - Dowiemy si� za chwil�. Po po�udniu pogadam z Wojciechowskim, chocia� uwa�am, �e to za wcze�nie, on tam musi rozpozna� znajomych i napuszczanie na niego tamtejszych ps�w nic nie da. Dopiero jak rozpozna, rozmowy z nimi za�atwi� szybciej, a szczerze m�wi�c, wola�bym po przyjacielsku. Szkoda, �e sam nie mog� tam skoczy�... Angielski znam s�abo, ale denatka wcale nie zna�a, to wszystko nasi.
<P align=justify>&nbsp; &nbsp; &nbsp; - A gdyby tak...? - o�ywi� si� Robert.
<P align=justify>&nbsp; &nbsp; &nbsp; - Czasu szkoda. Teraz natomiast, przypominam ci, przyjrzyjmy si� tej jakiej� Pawlakowskiej, kt�ra si� zapl�ta�a w poszukiwania i z kt�r� podobno gada� Hubek. Adres mamy, mo�e jeszcze aktualny, skoczysz tam.
<P align=justify>&nbsp; &nbsp; &nbsp; - Twierdzi�a, �e nic nie wie.
<P align=justify>&nbsp; &nbsp; &nbsp; - Twierdzi� mo�na du�o, a pogada� z ni� trzeba. Pawlakowska z m�a, pogadasz z tym m�em. Zaraz, mog� by� w pracy, o pracy danych nie mamy, to te� po po�udniu...
<P align=justify>&nbsp; &nbsp; &nbsp; - Czego w�a�ciwie szukamy?
<P align=justify>&nbsp; &nbsp; &nbsp; Bie�an zastanawia� si� przez chwil�.
<P align=justify>&nbsp; &nbsp; &nbsp; - Szukamy takiego, kt�ry odni�s� korzy�� ze �mierci Wandy Parker i mo�e odnie�� korzy�� ze �mierci Dorotki. To mog� by� dwie r�ne sprawy. Co do Dorotki, jedyn� rodzin� s� jej ciotki, ale jeszcze bli�sz� rodzin� stanowi ojciec, kt�ry nam tu nagle wyskoczy�. Ty si� zastan�w...
<P align=justify>&nbsp; &nbsp; &nbsp; Robert nie musia� si� nawet zastanawia�.
<P align=justify>&nbsp; &nbsp; &nbsp; - Odziedziczy�a spadek. Nie zabi�a testatorki, testament jest prawomocny. Gdyby nagle umar�a, po niej z kolei dziedziczy najbli�szy krewny, znaczy ojciec, prawnie uznany... Znaczy, rozumiesz, j�zyk mi si� pl�cze, nie ona jego uzna�a, tylko on j�, ale wystarczy...
<P align=justify>&nbsp; &nbsp; &nbsp; Zadzwoni� telefon. Bie�an zapisa� nazwiska, podane mu przez notariusza osobi�cie. Robert czeka�.
<P align=justify>&nbsp; &nbsp; &nbsp; - Rozumiem. Ma��e�stwo Kowalskich, Jolanta i Tadeusz, adres... Sprawa nieruchomo�ci, nie, to bez znaczenia. Hanna Wystrzyk, sprawa spadkowa, zamieszka�a Madali�skiego... I tylko oni? Nikt wi�cej? Tak, rozumiem, �e wizyt� pani Parker da�o si� dobrze zapami�ta�... Dzi�kuj� panu...
<P align=justify>&nbsp; &nbsp; &nbsp; - Trzy osoby, chwa�a Bogu, �e nie wi�cej - powiedzia� Robert, bior�c od Bie�ana kartk�. - O rany, ci Kowalscy w �omiankach! Ale maj� telefon, mo�na sprawdzi�, czy s� w domu.
<P align=justify>&nbsp; &nbsp; &nbsp; - No to ruszaj od razu - zarz�dzi� Bie�an. - Co widzieli, co s�yszeli, co komu nagadali i kiedy. Jak tam Ja�czak?
<P align=justify>&nbsp; &nbsp; &nbsp; - Dziesi�� minut temu jeszcze spa� - odpar�, podnosz�c s�uchawk�, Robert, kt�ry wzi�� na siebie obowi�zek pilnowania Marcinka. - Siostra i szwagier w pracy, dziecko w szkole. Uspokoi�em... Ca�e �omianki zaj�te. Uspokoi�em mamusi�, �e jest tylko wa�nym �wiadkiem i nic mu nie grozi... Brat w szkole. No, mam...!
<P align=justify>&nbsp; &nbsp; &nbsp; Pani Kowalska by�a w domu, Robert zatem od razu wyruszy� do �omianek, Bie�an za� na los szcz�cia uda� si� do pani Wystrzyk, kt�ra nie mia�a telefonu i kt�rej nie zasta�. Od s�siad�w dowiedzia� si�, �e pracuje jako sprz�taczka i bywa u r�nych ludzi. By�, oczywi�cie, po cywilnemu i, jak zwykle, robi� doskona�e wra�enie, przy okazji us�ysza� zatem, �e mo�e ju� nied�ugo pani Hani tej mord�gi, bo kroi jej si� spadek po ciotce, p� parceli i p� domu, za�atwia to w�a�nie, chocia� kto to tam wie, ci adwokaci to pod�e plemi�, a ona nie ma czym smarowa�. Informacji ch�tnie wys�ucha� i niezbicie stwierdzi�, �e szans� na uzupe�nienie jej zyska dopiero wieczorem.
<P align=justify>&nbsp; &nbsp; &nbsp; Robert w �omiankach dowiedzia� si�, �e pa�stwo Kowalscy u notariusza nic nie widzieli i nic nie s�yszeli, bo zbyt byli zaj�ci w�asnym problemem, nie wyobra�a sobie nawet, jaka to ko�omyja ze sprzeda�� jednego i kupnem drugiego, niby teraz to �atwo, a formalno�ci biurokratyczne wprost cz�owieka dusz�. I ka�dy ka�dego pr�buje oszuka�. Owszem, co� tam przeszkadza�o, ha�asy, nie, nie muzyka, zdaje si�, �e klientka tam jaka� by�a i dar�a si� takim cienkim g�osem, �e uszy puch�y. Starali si� nie s�ucha�, �eby w�asnego w�tku nie traci� i Bogiem a prawd� pani Kowalska jednego s�owa nie zapami�ta�a. A co do m�a, to tym bardziej nie, bo podobno kobiety na gadanie maj� podzieln� uwag�, m�czy�ni za� wr�cz przeciwnie.
<P align=justify>&nbsp; &nbsp; &nbsp; Stwierdzaj�c jej doskonale oboj�tny stosunek do klientki z cienkim g�osem, Robert w pe�ni uwierzy�, i� �adne miliony jej w ucho nie wpad�y. Nie mog�a zatem o nich rozpowiada�. Powiadomi� o tym Bie�ana w drodze powrotnej przez kom�rk� i otrzyma� polecenie dalszego pilnowania Marcinka. Na tym chwilowo stan�li, je�li nie liczy� kr�tkiego komunikatu z laboratorium, kt�ry dotar� do Bie�ana te� przez telefon.
<P align=justify>&nbsp; &nbsp; &nbsp; - Edziu - powiedzia� kumpel - mam pewno��, jeden �lad, chwyci� balustrad� ca�� r�k� w r�kawiczce, w jednym miejscu. Ot� to jest damska r�ka. �aden ch�op takich ma�ych r�czek nie posiada, chyba �e g�wniarz niedoros�y. Mo�e ci to co� da. Tam ci na biurko papier pos�a�em...
<P align=justify><CENTER><IMG height=16 src="graph/line16.gif" width=700 border=0><P>nast�pny&nbsp; &nbsp; &nbsp;
<A href="039.htm"><IMG height=32 src="graph/strz_prw.gif" width=32 border=0></A></CENTER></td></FONT>
<td width="120" background="graph/32.jpg"><br></td></tr>
<tr><td width="120"><img src="graph/13.jpg" width="120" height="120"></td>
<td width="100%" background="graph/23.jpg"><br></td>
<td width="120"><img src="graph/33.jpg" width="120" height="120"></td>
</tr></table></body></html>
