<HTML><HEAD>
<META http-equiv=Content-Type content="text/html; charset=iso-8859-2">
<META content="MW" name=GENERATOR></HEAD>
<body>
<font face="Times New Roman">
<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#E9E3C1">
<tr><td width="120"> <img src="graph/11.jpg" width="120" height="120"></td>
<td width="100%" background="graph/21.jpg"><br></td>
<td width="120"> <img src="graph/31.jpg" width="120" height="120"></td></tr>
<td width="120" background="graph/12.jpg"><br></td>
<td width="100%" background="graph/22.jpg"><BR>
<CENTER><FONT size=6>
Lew To�stoj&nbsp; &nbsp;
<A href="tyt.htm"><IMG height=32 src="graph/strz_gor.gif" width=32 border=0></A>&nbsp; &nbsp;
Wojna i pok�j - tom 3, cz�� 3
<P><IMG height=16 src="graph/line16.gif" width=700 border=0></P>
<A href="065.htm"><IMG height=32 src="graph/strz_lew.gif" width=32 border=0></A>
<IMG height=32 src="graph/bnc_anm.gif" width=32 border=0>
&nbsp; &nbsp; &nbsp; IV &nbsp; &nbsp; &nbsp;
<IMG height=32 src="graph/bnc_anm.gif" width=32 border=0>
<A href="067.htm"><IMG height=32 src="graph/strz_prw.gif" width=32 border=0></A>
<P><IMG height=16 src="graph/line16.gif" width=700 border=0>
</CENTER><small>
<P align=justify>&nbsp; &nbsp; &nbsp; <BIG><BIG>R</BIG></BIG>ada wojenna zebra�a si� o drugiej w obszernej �wietlicy ch�opa Andrzeja Sawostianowa. M�czy�ni, kobiety i dzieci du�ej ch�opskiej rodziny st�oczyli si� w kuchni po drugiej stronie sieni. Tylko wnuczka Andrzeja, sze�cioletnia Ma�asza, kt�r� jego dostojno�� upie�ci� i da� jej, pij�c herbat�, kawa�ek cukru, zosta�a na piecu w du�ej izbie. Ma�asza nie�mia�o i rado�nie patrzy�a z pieca na twarze, mundury i krzy�e genera��w, kt�rzy jeden po drugim wchodzili do izby i rozsiadali si� na szerokich lawach w k�cie pod ikonami. Sam "dziadunio" - Ma�asza tak nazwa�a Kutuzowa - siedzia� osobno w ciemnym k�cie za piecem. Siedzia� zapad�szy w sk�adany fotel, bez przerwy chrz�ka� i poprawia� ko�nierz surduta, kt�ry cho� rozpi�ty, jak gdyby uwiera� go w szyj�. Wchodz�cy podchodzili kolejno do feldmarsza�ka, niekt�rym �ciska� r�k�, innym kiwa� g�ow�. Adiutant Kajsarowa chcia� odsun�� firank� w oknie naprzeciw Kutuzowa, ale Kutuzow gniewnie machn�� r�k�. Kajsarow zrozumia� wi�c, �e jego dostojno�� nie chce, by widziano jego twarz.
<P align=justify>&nbsp; &nbsp; &nbsp; Doko�a ch�opskiego sosnowego sto�u, na kt�rym le�a�y mapy, plany, o��wki, papiery, zebra�o si� tylu ludzi, �e ordynansi przynie�li jeszcze jedn� �aw� i postawili przy stole. Na tej �awce usiedli Jermo�ow, Kajsarow i Toll, kt�rzy przybyli ostatni. Pod ikonami, na honorowym miejscu, siedzia� Barclay de Tolly z Krzy�em �w. Jerzego na szyi, twarz mia� blad�, cierpi�c�, a jego wysokie czo�o zlewa�o si� z �ys� g�ow�. Ju� drugi dzie� dr�czy�a go febra, teraz w�a�nie mia� dreszcze i �amanie w ko�ciach. Obok niego siedzia� Uwarow i niezbyt g�o�no (wszyscy tak m�wili), �ywo gestykuluj�c, m�wi� co� do Barclaya. Niziutki, okr�glutki Dochturow, uni�s�szy brwi i z�o�ywszy r�ce na brzuchu, uwa�nie si� przys�uchiwa�. Z drugiej strony siedzia� hrabia Ostermann-To�stoj, wspar� na d�oni szerok� g�ow� o �mia�ych rysach i b�yszcz�cych oczach i wydawa� si� pogr��ony w my�lach. Rajewski z wyrazem zniecierpliwienia, zwyk�ym gestem mierzwi�c na skroniach swe czarne w�osy spogl�da� to na Kutuzowa, to na drzwi. Stanowcza, pi�kna i dobrotliwa twarz Konownicyna ja�nia�a subtelnym, filuternym u�miechem. Pochwyci� spojrzenie Ma�aszy i oczami dawa� jej znaki, kt�re �mieszy�y dziewczynk�.
<P align=justify>&nbsp; &nbsp; &nbsp; Wszyscy czekali na Bennigsena, kt�ry pod pozorem nowego przegl�du pozycji ko�czy� smaczny obiad. Czekano na niego od czwartej do sz�stej i przez ca�y ten czas nie przyst�powano do narady, tylko po cichu rozmawiano o r�nych sprawach.
<P align=justify>&nbsp; &nbsp; &nbsp; Dopiero kiedy do izby wszed� Bennigsen, Kutuzow wysun�� si� ze swego k�ta i zbli�y� si� do sto�u, ale na tak� odleg�o��, �e ustawione na stole �wiece nie o�wietla�y jego twarzy.
<P align=justify>&nbsp; &nbsp; &nbsp; Bennigsen zagai� narad� pytaniem: "Odda� bez walki �wi�t� i staro�ytn� stolic� Rosji czy te� jej broni�?" Nast�pi�o d�ugie, og�lne milczenie. Wszystkie twarze zas�pi�y si�, a w ciszy s�ycha� by�o gniewne chrz�kanie i pokas�ywanie Kutuzowa. Wszystkie oczy patrzy�y na niego. Ma�asza r�wnie� patrzy�a na dziadunia. By�a najbli�ej i widzia�a, jak krzywi� twarz, jakby zbiera�o mu si� na p�acz. Ale to trwa�o nied�ugo.
<P align=justify>&nbsp; &nbsp; &nbsp; <b>- �wi�t�, staro�ytn� stolic� Rosji!</b> - odezwa� si� nagle, gniewnym g�osem powtarzaj�c s�owa Bennigsena i w ten spos�b podkre�laj�c fa�szyw� nut� tych s��w. - Pozwolisz, ekscelencjo, i� rzekn�, �e to pytanie nie ma sensu dla Rosjanina. (Przewali� si� naprz�d swym ci�kim cia�em.) Takiego pytania nie nale�y stawia� i takie pytanie nie ma sensu. Zagadnienie, z powodu kt�rego prosi�em, by ci panowie si� zebrali, jest zagadnieniem wojskowym. Kwestia stoi tak: "Ocalenie Rosji jest w armii. Co jest tedy korzystniejsze: zaryzykowa� strat� armii i Moskwy, je�li si� przyjmie bitw�, czy te� odda� Moskw� bez walki?" W tej w�a�nie kwestii pragn� pozna� zdanie pan�w.
<P align=justify>&nbsp; &nbsp; &nbsp; Odchyli� si� w ty� na oparcie fotela.
<P align=justify>&nbsp; &nbsp; &nbsp; Zacz�a si� dyskusja. Bennigsen nie uwa�a�, �e partia zosta�a przegrana. Ust�puj�c zdaniu Barclaya oraz innych co do mo�liwo�ci przyj�cia bitwy obronnej pod Pi�ami, przej�ty rosyjskim patriotyzmem i mi�o�ci� do Moskwy, proponowa� przeprowadzenie noc� wojsk z prawego skrzyd�a na lewe i uderzenie nazajutrz na prawe skrzyd�o Francuz�w. Zdania si� podzieli�y, wypowiadano si� za tym planem i przeciwko niemu. Jermo�ow, Dochturow i Rajewski zgadzali si� ze zdaniem Bennigsena. Ci genera�owie, czy to uwa�aj�c, �e nale�y z�o�y� ofiar� przed oddaniem stolicy, czy te� powodowani jakimi� wzgl�dami osobistymi, nie rozumieli jak gdyby, �e obecna narada nie zmieni nieuniknionego biegu spraw i �e Moskwa ju� jest oddana.
<P align=justify>&nbsp; &nbsp; &nbsp; Inni genera�owie rozumieli to, pozostawiaj�c wi�c na uboczu spraw� Moskwy, m�wili o kierunku, w jakim powinno si� cofa� wojsko.
<P align=justify>&nbsp; &nbsp; &nbsp; Ma�asza, nie spuszczaj�c oczu z tego, co si� przed ni� dzieje, inaczej pojmowa�a znaczenie tej narady. Wydawa�o si� jej, i� rzecz polega na osobistej walce mi�dzy "dziaduniem" a "d�ugopo�ym", jak nazywa�a Bennigsena. Widzia�a, �e si� z�oszcz�, kiedy ze sob� rozmawiaj�, i w duchu trzyma�a stron� dziadunia. Zauwa�y�a, �e w�r�d rozmowy dziadunio rzuci� na Bennigsena szybkie, drwi�ce spojrzenie, a potem ku swej rado�ci spostrzeg�a, �e dziadunio powiedzia� co� d�ugopo�emu i osadzi� go; Bennigsen poczerwienia� nagle i przeszed� si� po izbie.
<P align=justify>&nbsp; &nbsp; &nbsp; S�owami, kt�re tak podzia�a�y na Bennigsena, by�o zdanie Kutuzowa wypowiedziane g�osem cichym i spokojnym o zaletach i wadach propozycji Bennigsena, polegaj�cej na przeprowadzeniu noc� wojsk z prawego skrzyd�a na lewe w celu zaatakowania prawego skrzyd�a Francuz�w.
<P align=justify>&nbsp; &nbsp; &nbsp; - Panowie - o�wiadczy� Kutuzow - nie mog� zaaprobowa� planu hrabiego. Ruch wojsk w bliskiej odleg�o�ci od nieprzyjaciela jest zawsze niebezpieczny, historia wojen potwierdza to mniemanie. Na przyk�ad... - Kutuzow jak gdyby si� zamy�li� szukaj�c przyk�adu i patrzy� na Bennigsena jasnym, naiwnym spojrzeniem. - Cho�by bitwa pod Friedlandem, kt�r�, jak s�dz�, hrabia dobrze pami�ta, by�a... niezupe�nie udana tylko dlatego, �e nasze wojska zmieni�y uszykowanie w nader bliskiej odleg�o�ci od nieprzyjaciela...
<P align=justify>&nbsp; &nbsp; &nbsp; Nast�pi�a chwila milczenia, kt�re wszystkim wyda�o si� bardzo d�ugie.
<P align=justify>&nbsp; &nbsp; &nbsp; Debaty wznowiono, ale cz�sto nast�powa�y przerwy i wszyscy czuli, �e ju� nie ma o czym m�wi�.
<P align=justify>&nbsp; &nbsp; &nbsp; Podczas jednej z takich przerw Kutuzow ci�ko westchn��, jakby zamierza� przem�wi�. Wszyscy spojrzeli na niego.
<P align=justify>&nbsp; &nbsp; &nbsp; - <I>Eh hien, messieurs! Je vois que c'est moi quipayerai lespots casses</I><A href="PRZYPIS\0155.htm">*</A> - powiedzia�. Uni�s� si� powoli i zbli�y� do sto�u. - Panowie, wys�ucha�em waszych opinii. Niekt�rzy nie zgodz� si� ze mn�. Wszelako ja (przerwa) na mocy w�adzy danej mi przez mego monarch� i ojczyzn�, ja, nakazuj� odwr�t.
<P align=justify>&nbsp; &nbsp; &nbsp; Zaraz potem genera�owie zacz�li si� rozchodzi� z tak� sam� uroczyst� i milcz�c� ostro�no�ci�, z jak� si� zwyk�o rozchodzi� po pogrzebie.
<P align=justify>&nbsp; &nbsp; &nbsp; Niekt�rzy z genera��w �ciszonym g�osem, w zupe�nie innym diapazonie, ni� przemawiali na naradzie, co� wodzowi naczelnemu meldowali.
<P align=justify>&nbsp; &nbsp; &nbsp; Ma�asza - ju� dawno wo�ano j� na kolacj� - ostro�nie, ty�em zesz�a z przypiecka czepiaj�c si� wyst�p�w pieca bosymi no�ynami i pl�cz�c si� mi�dzy nogami genera��w smyrgn�la w drzwi.
<P align=justify>&nbsp; &nbsp; &nbsp; Po odprawieniu genera��w Kutuzow d�ugo siedzia� wsparty �okciami o st�, ci�gle rozmy�la� nad tym strasznym pytaniem: "Kiedy, kiedy wreszcie zosta�o postanowione oddanie Moskwy? Kiedy sta�o si� to, co rozstrzygn�o spraw�, i kto jest temu winien?"
<P align=justify>&nbsp; &nbsp; &nbsp; - Tegom to si� nie spodziewa� - rzek� do adiutanta Sznajdra, kt�ry p�n� noc� wszed� do niego. - Tegom si� nie spodziewa�! Tegom nie my�la�!
<P align=justify>&nbsp; &nbsp; &nbsp; - Trzeba odpocz��, wasza dostojno�� - rzek� Sznajder.
<P align=justify>&nbsp; &nbsp; &nbsp; - W�a�nie �e nie! B�d� oni jeszcze �re� mi�so ko�skie, jak Turcy - krzykn�� Kutuzow nie odpowiadaj�c mu i pulchn� pi�ci� uderzy� w st�. - B�d� i oni, �eby tylko...
<P align=justify><CENTER><IMG height=16 src="graph/line16.gif" width=700 border=0><P>nast�pny&nbsp; &nbsp; &nbsp;
<A href="067.htm"><IMG height=32 src="graph/strz_prw.gif" width=32 border=0></A></CENTER></td></FONT>
<td width="120" background="graph/32.jpg"><br></td></tr>
<tr><td width="120"><img src="graph/13.jpg" width="120" height="120"></td>
<td width="100%" background="graph/23.jpg"><br></td>
<td width="120"><img src="graph/33.jpg" width="120" height="120"></td>
</tr></table></body></html>
